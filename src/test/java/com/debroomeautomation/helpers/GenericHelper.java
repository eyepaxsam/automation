package com.debroomeautomation.helpers;

import com.debroomeautomation.settings.ObjectRepository;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class GenericHelper {
    public static void waitUntilElementToBeVisible(WebElement element){
        ObjectRepository.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        Wait<WebDriver> wait = new FluentWait<WebDriver>(ObjectRepository.driver)
                .withTimeout(Duration.ofSeconds(30))
                .pollingEvery(Duration.ofSeconds(2))
                .ignoring(NoSuchElementException.class);
        WebElement elm = wait.until(driver -> element);
    }
    public static boolean IsElementDisplayed(WebElement element){
        return element.isDisplayed();
    }
}
