package com.debroomeautomation.configurations;

public enum BrowserTypes {
    Firefox,
    Chrome,
    Opera,
    InternetExplorer
}
