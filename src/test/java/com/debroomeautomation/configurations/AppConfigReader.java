package com.debroomeautomation.configurations;

import com.debroomeautomation.settings.AppConfigKeys;
import com.debroomeautomation.interfaces.IConfig;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class AppConfigReader implements IConfig {
    private final Properties properties = new Properties();
    private final String userDir;

    public AppConfigReader() throws IOException {
        String configPath = "\\src\\test\\java\\resources\\global.properties";
        userDir = System.getProperty("user.dir");
        FileInputStream inputStream = new FileInputStream(userDir + configPath);
        properties.load(inputStream);
    }

    @Override
    public BrowserTypes getBrowserTypes() {
        String browser = properties.getProperty(AppConfigKeys.browser);
        return BrowserTypes.valueOf(browser);
    }

    @Override
    public String getEnvironment() {
        return properties.getProperty(AppConfigKeys.environment);
    }

    @Override
    public String getURL() {
        return properties.getProperty(AppConfigKeys.url);
    }

   /* @Override
    public String getHubUrl(){
        return properties.getProperty(AppConfigKeys.hubUrl);
    }*/
}
