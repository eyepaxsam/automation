package com.debroomeautomation.support;


import io.cucumber.java.After;

public class Hook extends BaseClass{
    @After
    public void executeTearDown(){
        tearDown();
    }
}
