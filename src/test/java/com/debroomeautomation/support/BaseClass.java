package com.debroomeautomation.support;

import com.debroomeautomation.configurations.AppConfigReader;
import com.debroomeautomation.exceptions.DriverTypeUndefinedException;
import com.debroomeautomation.settings.ObjectRepository;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.io.IOException;

public class BaseClass {
    private static WebDriver driver;


    //Get browser
    public static WebDriver getChrome() throws IOException {
        System.setProperty("webdriver.chrome.driver","D:\\Workspace-debroome\\driver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        return driver;
    }
    public static WebDriver getFireFox() throws IOException {
        System.setProperty("webdriver.chrome.driver","D:\\Workspace-debroome\\driver\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        return driver;
    }
    public static WebDriver getInternetExplorer() throws IOException {
        System.setProperty("webdriver.chrome.driver","D:\\Workspace-debroome\\driver\\chromedriver.exe");
        WebDriver driver = new InternetExplorerDriver();
        return driver;
    }
    public static WebDriver initWebDriver() throws IOException, DriverTypeUndefinedException {
        ObjectRepository.config = new AppConfigReader();
        switch (ObjectRepository.config.getBrowserTypes()){
            case Chrome:
                ObjectRepository.driver = getChrome();
                return ObjectRepository.driver;
            case Firefox:
                ObjectRepository.driver = getFireFox();
                return ObjectRepository.driver;
            case InternetExplorer:
                ObjectRepository.driver = getInternetExplorer();
                return ObjectRepository.driver;
            case Opera:
                return null;
            default:
                throw new DriverTypeUndefinedException("Invalid Driver Name Specified :"
                        + ObjectRepository.config.getBrowserTypes().toString());
        }

    }
    public void tearDown(){
            ObjectRepository.driver.close();
            ObjectRepository.driver.quit();
    }
}
