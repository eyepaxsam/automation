package com.debroomeautomation.interfaces;

import com.debroomeautomation.configurations.BrowserTypes;

public interface IConfig {
    BrowserTypes getBrowserTypes();
    String getEnvironment();
    String getURL();
    //String getHubUrl();
}
