Feature: As an Admin user i should be validate in the login page
Background:
  Given I am on manual login page

  @all @regression
  Scenario Outline: As an Admin i should not be able to login using invalid credentials
    When I enter the username "<username>" on username field
    And I enter the password "<password>" on password field
    And I click on login button
    Then I should see "<validation>" validation message

    Examples:
      |username|password|validation|
      |sameera.s@eyepax.com|12345|The email address or password is incorrect|
      |sameera@eyepax.com|eyep@x123|The email address or password is incorrect|

  @all @regression
  Scenario Outline: As an Admin I should be able to enter valid username and login
    When I enter the username "<username>" on username field
    And I enter the password "<password>" on password field
    And I click on login button
    Then I should get navigated to Home screen and see admin panel


    Examples:
      |username|password|
      |sameera.s@eyepax.com|eyep@x123|