package com.debroomeautomation.pageObjects;

import com.debroomeautomation.helpers.GenericHelper;
import com.debroomeautomation.settings.ObjectRepository;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPageObject {
    public LoginPageObject(){
        PageFactory.initElements(ObjectRepository.driver,this);
    }

    @FindBy(xpath = "//*[@id='email']")
    private WebElement usernameTxtFld;
    @FindBy(xpath = "//*[@id='password']")
    private WebElement userpasswordTextFld;
    @FindBy(xpath = "//input[@class='submit']")
    private WebElement loginBtn;
    @FindBy(xpath = "//*[@id='main']/div[1]/form/p")
    private WebElement errorMsg;
    @FindBy(xpath = "//div[@id='adminpanel']")
    private WebElement adminPanel;

    public LoginPageObject setUsernameTxt(String username) {
        GenericHelper.waitUntilElementToBeVisible(usernameTxtFld);
        this.usernameTxtFld.sendKeys(username);
        return this;
    }

    public LoginPageObject serUserpasswordText(String password){
        this.userpasswordTextFld.sendKeys(password);
        return this;
    }

    public void errorValidation(String msg){
        GenericHelper.waitUntilElementToBeVisible(errorMsg);
        Assert.assertEquals(msg,errorMsg.getText());
    }

    public LoginPageObject login(){
        this.loginBtn.click();
        return this;
    }

    public void validateUser(){
        GenericHelper.waitUntilElementToBeVisible(adminPanel);
    }
}
