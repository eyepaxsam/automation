package com.debroomeautomation.stepdefinitions;

import com.debroomeautomation.configurations.AppConfigReader;
import com.debroomeautomation.exceptions.DriverTypeUndefinedException;
import com.debroomeautomation.helpers.BrowserHelper;
import com.debroomeautomation.pageObjects.LoginPageObject;
import com.debroomeautomation.support.BaseClass;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.percy.selenium.Percy;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class LoginStepDefinition extends BaseClass {
    private final WebDriver driver = initWebDriver();
    private final LoginPageObject loginPageObject = new LoginPageObject();
    private Percy percy;

    public LoginStepDefinition() throws IOException, DriverTypeUndefinedException {
    }

    @Given("I am on manual login page")
    public void iAmOnManualLoginPage() throws IOException{
        AppConfigReader appConfigReader =new AppConfigReader();
        assert driver !=null;
        driver.get(appConfigReader.getURL());
        BrowserHelper.maximize();
        percy = new Percy(driver);
        percy.snapshot("Login Page - Username");
    }

    @When("I enter the username {string} on username field")
    public void iEnterTheUsernameOnUsernameField(String username) {
        loginPageObject.setUsernameTxt(username);
    }

    @And("I enter the password {string} on password field")
    public void iEnterThePasswordOnPasswordField(String userpassword) {
        loginPageObject.serUserpasswordText(userpassword);
    }

    @And("I click on login button")
    public void iClickOnLoginButton(){
        loginPageObject.login();
    }

    @Then("I should see {string} validation message")
    public void iShouldSeeValidationMessage(String validationMsg) {
        percy.snapshot("Login - Incorrect credentials");
        loginPageObject.errorValidation(validationMsg);
    }

    @Then("I should get navigated to Home screen and see admin panel")
    public void iShouldGetNavigatedToHomeScreenAndSeeAdminPanel() {
        loginPageObject.validateUser();
        percy.snapshot("Successful user login");
    }
}
