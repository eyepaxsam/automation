package com.debroomeautomation.stepdefinitions;

import com.debroomeautomation.support.BaseClass;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

import java.io.IOException;

public class CommonSteps extends BaseClass {
    public CommonSteps() throws IOException {
    }

    @Then("I wait for {int} seconds")
    public void i_wait_for_seconds(int seconds) throws InterruptedException {
        Thread.sleep(seconds*1000);
    }


}
