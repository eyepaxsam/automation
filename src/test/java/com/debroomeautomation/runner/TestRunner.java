package com.debroomeautomation.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/com/debroomeautomation/features",
        glue = {"com.debroomeautomation.stepdefinitions","com.debroomeautomation.support"},
        monochrome = true,
        plugin = {"pretty","html:target/cucumber.html","json:target/cucumber.json","junit:target/cukes.xml"},
        tags = "@all")
public class TestRunner {
}

