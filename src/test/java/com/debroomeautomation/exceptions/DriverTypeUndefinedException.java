package com.debroomeautomation.exceptions;

public class DriverTypeUndefinedException extends Exception {
    public DriverTypeUndefinedException(String message){
        super(message);
    }
}
