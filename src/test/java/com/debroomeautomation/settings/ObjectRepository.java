package com.debroomeautomation.settings;

import com.debroomeautomation.configurations.AppConfigReader;
import com.debroomeautomation.interfaces.IConfig;
import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.WebDriver;

@Getter
@Setter
public class ObjectRepository {
    public static IConfig config;
    public static WebDriver driver;
}
